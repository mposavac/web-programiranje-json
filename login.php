<?php
$data = json_decode($_POST["x"], false);

$email = $data->email;
$pswd = $data->pswd;
$obj = new \stdClass();

if (strlen($email) == 0 || strlen($pswd) == 0){
    header("HTTP/1.1 401 Unauthorized");
    $obj->error = "Email or password are missing.";
    $JSON = json_encode($obj);
    echo $JSON;
    exit;
}
else{
    $obj->username = substr($email, 0, strrpos($email, '@', 0));
    $obj->city = $data->city;
    $JSON = json_encode($obj);
    echo $JSON;
    exit;
}
?>