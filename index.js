const form = document.querySelector("#form");
const paragraph = document.querySelector(".server_response");

form.addEventListener("submit", e => {
  e.preventDefault();

  const email = form.querySelector("#inputEmail").value;
  const pswd = form.querySelector("#inputPassword").value;
  const address = form.querySelector("#inputAddress").value;
  const city = form.querySelector("#inputCity").value;
  const country = form.querySelector("#inputCountry").value;
  const zip = form.querySelector("#inputZip").value;
  const data = {
    email: email,
    pswd: pswd,
    add: address,
    city: city,
    country: country,
    zip: zip
  };

  const param = JSON.stringify(data);
  const xmlhttp = new XMLHttpRequest();

  xmlhttp.open("POST", "login.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      let responseJSON = JSON.parse(this.responseText);
      paragraph.innerHTML =
        "WELCOME " + responseJSON.username + "(" + responseJSON.city + ")";
    } else if (this.readyState == 4 && this.status == 401) {
      let responseJSON = JSON.parse(this.responseText);
      paragraph.innerHTML = responseJSON.error;
    } else paragraph.innerHTML = "LOGIN FAILED";
  };

  xmlhttp.send("x=" + param);
});
